import { render, screen, fireEvent, waitFor, getByText, cleanup } from '@testing-library/vue'
import { test, expect } from 'vitest'
import '@testing-library/jest-dom'

import AddTodo from './components/AddTodo.vue'
import TodoList from './components/TodoList.vue'

test('AddTodo component', async () => {
  const fakeDescription = 'Testing Description1'
  const {getByRole, getByPlaceholderText} = render(AddTodo)

  // adding description
  const input = getByPlaceholderText('Enter description...')
  await fireEvent.update(input, fakeDescription)
  expect(input.value).toBe(fakeDescription)

  // clear description
  await fireEvent.click(getByRole('clear'))
  expect(input.value).toBe('')
})

test('TodoList component', async () => {
  render(AddTodo)
  const item = {description: 'Testing', isCompleted: false}
  const {getByRole} = render(TodoList)
  const status = false
  
  const fakeDescription = 'Testing Description1'

  // adding description
  const input = screen.getByPlaceholderText('Enter description...')
  await fireEvent.update(input, fakeDescription)
  expect(input.value).toBe(fakeDescription)

  await fireEvent.click(
    screen.getByRole('add-item')
  ).then(()=> console.log('111111'))

  // await fireEvent.click(getByRole('mark-complete'))
  // await fireEvent.click(getByRole('delete'))
})
